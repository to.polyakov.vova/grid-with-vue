## Description 
This is and example of how css grid can be easily used to place content on the page =) 
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
``` 

### Compiles and minifies for production
```
npm run build
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
