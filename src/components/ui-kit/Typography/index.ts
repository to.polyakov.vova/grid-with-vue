import Text from './Text/Text.vue'
import H1 from './h1/H1.vue'
import H2 from './h2/H2.vue'
import H3 from './h3/H3.vue'
import H4 from './h4/H4.vue'
import H5 from './h5/H5.vue'
import H6 from './h6/H6.vue'
import H7 from './h7/H7.vue'

export { Text, H1, H2, H3, H4, H5, H6, H7 }
