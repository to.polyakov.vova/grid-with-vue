declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

declare module '*.scss' {
  interface Styles {
    [key: string]: string;
  }

  const styles: Styles
  export = styles;
}
