export const HEADER_TEXT = {
  H1: 'Header h1',
  H2: 'Header h2',
  H3: 'Header h3',
  H4: 'Header h4',
  H5: 'Header h5',
  H6: 'Header h6'
}
export const EXAMPLE_TEXT = 'Exxample of the grid layout \n try it out'

export const DEFAULT = 'Default'
export const P_COPMOSITION = 'Paragraph composition'
export const TYPOGRAPHY = 'Typography'

export const WIDE = 'Wide'
export const SMALL_FONT = 'Small Font'
export const PARAGRAPH_TEXT = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab assumenda, commodi consectetur corporis esse inventore iusto minima officiis omnis ratione tempore temporibus. Aspernatur beatae excepturi ipsam iure, mollitia nulla praesentium ratione veritatis voluptates! Alias autem consequatur doloremque eum non numquam.'
